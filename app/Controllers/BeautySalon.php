<?php 
namespace App\Controllers;

use App\Models\Product;
use App\Models\User;
use App\Models\UserInfo;
use App\Models\Feedback;
use App\Models\Appointment;
use App\Models\Services;
use App\Models\Receipt;
use CodeIgniter\I18n\Time;

class BeautySalon extends BaseController{

    public $categoryArray = array(
        '1' => 'Rostro',
        '2' => 'Ojos',
        '3' => 'Pestañas',
        '4' => 'Corporal',
        '5' => 'Labios',
    );

    public function calculateDiscount($user){
        $appointmentdb = new Appointment();
        $current = new Time('now');
        $discount = .0;
        $appointments['userAppointments'] = $appointmentdb->getAppointmentsByUser($user);
        $data['example'] = [];
        $numAppointments = [
            'week' => 0,
            'month' => 0,
            'year' => 0
        ];
        foreach ($appointments['userAppointments'] as $aux){
            $myTime = Time::parse($aux['fecha'],'America/Mexico_City', 'en_US');
            $diff = $current->difference($myTime);
            if ($diff->getWeeks()==0){
                $numAppointments['week']++;
            }
            if ($diff->getMonths()==0){
                $numAppointments['month']++;
            }
            if ($diff->getYears()==0){
                $numAppointments['year']++;
            }
        }
        if ($numAppointments['week']>3){
            $discount = .1;
        }
        if ($numAppointments['month']>5){
            $discount = .2;
        }
        if ($numAppointments['year']>20){
            $discount = .3;
        }
        $session = session();
        $session->set('discount', $discount);
    }

    public function registerUser(){
        $userPost = $this->request->getPost();
        $userdb = new User();
        $userinfo = new UserInfo();
        $userPost['password'] = password_hash($userPost['password'], PASSWORD_DEFAULT);
        $user_verify = $userdb->getUser(['usuario' => $userPost['usuario']]);
        $session = session();
        if ($user_verify!=null){
            $session->setFlashdata('error', 'Este usuario ya existe');
            return redirect()->to(base_url('/registerpage'));
        } else {
            $userdb->insertUser($userPost);
            $userinfo->insertUserInfo($userPost);
            return redirect()->to(base_url('/loginpage'));
        }
    }

    public function processLogin(){
        $userPost = $this->request->getPost();
        $userdb = new User();
        $data['errors'] = [];

        $user_data = $userdb->getUser(['usuario' => $userPost['usuario']]);
        $session = session();
        if ($user_data!=null && password_verify($userPost['password'], $user_data['password'])){
            $data = [
                "user" => $user_data['usuario'],
                "type" => $user_data['type'],
                "products" => [],
                "appointments" => [],
            ];
            $this->calculateDiscount($user_data['usuario']);
            $session->set($data);
            return redirect()->to(base_url('/homepage'));
        } else {
            if ($user_data!=null){
                $session->setFlashdata('error', 'Contraseña incorrecta');
                return redirect()->to(base_url('/loginpage'));
            } else {
                $session->setFlashdata('error', 'Este usuario no existe');
                return redirect()->to(base_url('/loginpage'));
            }
        }
    }

    public function loginpage(){
        return view('Common/common')
            . view('Common/header')
            . view('Common/menu')
            .view('BeautySalonViews/loginpage')
            . view('Common/footer');
    }
    
    public function registerpage(){
        return view('Common/common')
            . view('Common/header')
            . view('Common/menu')
            .view('BeautySalonViews/register')
            . view('Common/footer');
    }

    public function processOrder($total){
        $receiptdb = new Receipt();
        $receiptData = [
            'usuario' => session('user'),
            'productos' => $this->jsonProducts(),
            'citas' => $this->jsonAppointments(),
            'total' => $total,
            'discount' => session('discount')
        ];
        $this->orderAppointment();
        $receiptdb->insertReceipt($receiptData);
        foreach ($_SESSION['products'] as $key => &$val){
            unset($_SESSION['products'][$key]);
        }
        foreach ($_SESSION['appointments'] as $key => &$val){
            unset($_SESSION['appointments'][$key]);
        }
        return redirect()->to(base_url('/homepage'));
    }

    public function jsonAppointments(){
        $appointment = new Services();
        $data= [];
        if (!empty(session('appointments'))){
            foreach (session('appointments') as $cartService){
                $array = $appointment->getServiceByID($cartService['id_servicio']);
                $array['fecha']= $cartService['fecha'];
                $array['hora_inicio'] = $cartService['hora_inicio'];
                $array['hora_fin'] = $cartService['hora_fin'];
                array_push($data, $array);
            }
        }
        return json_encode($data);
    }

    public function jsonProducts(){
        $product = new Product();
        $data = [];
        if (!empty(session('products'))){
            foreach (session('products') as $cartProduct){
                
                $array = $product->getProductByID($cartProduct['id']);
                $array['cantidad']= $cartProduct['cantidad'];
                $array['total'] = floatval($array['cantidad']) * floatval($array['precio']) ;
                array_push($data, $array);
            }
        }
        return json_encode($data);
    }

    public function orderAppointment(){
        $appointment = new Appointment();
        if (session('appointments')!=null){
            foreach ($_SESSION['appointments'] as $key => &$val){
                $appointment->insertAppointment($_SESSION['appointments'][$key]);
            }
        }
    }

    public function removeProduct($id){
        if (!empty(session('products'))){
            foreach ($_SESSION['products'] as $key => &$val){
                if ($_SESSION['products'][$key]['id']==$id){
                    unset($_SESSION['products'][$key]);
                    return redirect()->to(base_url('/shoppingcart'));
                }
            }
        }
    }

    public function removeAppointment($id){
        if (!empty(session('appointments'))){
            foreach ($_SESSION['appointments'] as $key => &$val){
                if ($_SESSION['appointments'][$key]['id_servicio']==$id){
                    unset($_SESSION['appointments'][$key]);
                    return redirect()->to(base_url('/shoppingcart'));
                }
            }
        }
    }

    public function addProduct($id){
        $userPost = $this->request->getPost();
        $array = [
            array(
                'id' => $id,
                'cantidad' => $userPost['cantidad']
            )
        ];
        if(session('user')!=null){
            if (!empty(session('products'))){
                foreach ($_SESSION['products'] as $key => &$val){
                    if (($_SESSION['products'][$key]['id'])==$id && ($_SESSION['products'][$key]['cantidad']+$userPost['cantidad'])<=10){
                        $_SESSION['products'][$key]['cantidad'] += $userPost['cantidad'];
                        return redirect()->to(base_url('/shoppingcart'));
                    } else if (($_SESSION['products'][$key]['cantidad']+$userPost['cantidad'])>10){
                        return redirect()->to(base_url('/shoppingcart'));
                    }
                }
            }
            $session = session();
            $session->push('products', $array);
            return redirect()->to(base_url('/shoppingcart'));
        } else {
            return redirect()->to(base_url('/loginpage'));
        }
    }

    public function proccessDate($id_servicio){
        $service = new Services();
        $appointment = new Appointment();
        $open_time = new Time('09:00');
        $close_time = new Time('17:00');
        $duration = $service->getDurationByID($id_servicio);
        $userPost = $this->request->getPost();
        $userPost['fecha'] = date("Y-m-d", strtotime($userPost['fecha']));
        $aux['horarios'] = [];
        $aux['fecha'] = $userPost['fecha'];
        $aux['servicio'] = $service->getServiceByID($id_servicio);

        do{
            $array = $appointment->getAppointmentsByDate($id_servicio, $userPost['fecha'], $open_time->toTimeString());
            if ($array==null){
                $a = [
                    'hora_inicio' => $open_time->toTimeString()
                ];
                $open_time = $open_time->addMinutes($duration['duracion']);
                $a['hora_fin'] = $open_time->toTimeString();
                array_push($aux['horarios'], $a);
            } else {
                $open_time = $open_time->addMinutes($duration['duracion']);
            }
        }while(!$open_time->isAfter($close_time));

        return view('Common/common')
            . view('Common/header')
            . view('Common/menu')
            . view('BeautySalonViews/scheduletime', $aux)
            . view('Common/footer');
    }

    public function proccessAppointment($id_servicio){
        $service = new Services();
        $userPost = $this->request->getPost();
        $duration = $service->getDurationByID($id_servicio);
        $myTime = new Time($userPost['hora_inicio']);
        $userPost['hora_inicio'] = $myTime->toTimeString();
        $myTime=$myTime->addMinutes($duration['duracion']);
        $userPost['hora_fin'] = $myTime->toTimeString();
        $userPost['usuario'] = session('user');
        $array = [
            array(
                'fecha' => $userPost['fecha'],
                'id_servicio' => $id_servicio,
                'hora_inicio' => $userPost['hora_inicio'],
                'hora_fin' => $userPost['hora_fin'],
                'usuario' => $userPost['usuario'],
            )
        ];
        
        if(session('user')!=null){
            $session = session();
            $session->push('appointments', $array);
            return redirect()->to(base_url('/shoppingcart'));
        } else {
            return redirect()->to(base_url('/loginpage'));
        }
    }

    public function fetchAppointments(){
        $product = new Services();
        $data['appointments'] = [];
        if (!empty(session('appointments'))){
            foreach (session('appointments') as $cartService){
                $array = $product->getServiceByID($cartService['id_servicio']);
                $array['fecha']= $cartService['fecha'];
                $array['hora_inicio'] = $cartService['hora_inicio'];
                $array['hora_fin'] = $cartService['hora_fin'];
                array_push($data['appointments'], $array);
            }
        }
        return $this->response->setJSON($data);
    }

    public function logout(){
        $session = session();
        $session->destroy();
        return redirect()->to(base_url('/loginpage'));
    }

    public function proccessFeedback(){
        $feedback = new Feedback();
        $data['errors']= [];
        if ($this->request->getPost()!=null){
            if ($this->validate($feedback->validationRules)){
                $feedback->insertFeedback($this->request->getPost());
            } 
        } 
        return view('Common/common')
            . view('Common/header')
            . view('Common/menu')
            .view('BeautySalonViews/feedbacksuccess')
            . view('Common/footer');
    }

    
    
    public function schedule($id_servicio){
        $service = new Services();
        $data['servicio'] = $service->getServiceByID($id_servicio);
        return view('Common/common')
            . view('Common/header')
            . view('Common/menu')
            .view('BeautySalonViews/schedule', $data)
            . view('Common/footer');
    }

    public function scheduletime($id_servicio){
        $service = new Services();
        $data['servicio'] = $service->getServiceByID($id_servicio);
        return view('Common/common')
            . view('Common/header')
            . view('Common/menu')
            .view('BeautySalonViews/scheduletime', $data)
            . view('Common/footer');
    }

    public function services(){
        $data['common']= view ('Common/common');
        $data['header']= view('Common/header');
        $data['menu']= view('Common/menu');
        $data['footer']= view('Common/footer');
        return view('BeautySalonViews/services', $data);
    }

    public function gallery(){
        return view('Common/common')
            . view('Common/header')
            . view('Common/menu')
            .view('BeautySalonViews/gallery')
            . view('Common/footer');
    }
    
    public function shoppingcart(){
        if (session('user')!=null){
            $this->calculateDiscount(session('user'));
        }
        
        $data['common']= view ('Common/common');
        $data['header']= view('Common/header');
        $data['menu']= view('Common/menu');
        $data['footer']= view('Common/footer');
        return view('BeautySalonViews/shoppingcart', $data);
    }

    public function feedbackPage(){
        return view('Common/common')
            . view('Common/header')
            . view('Common/menu')
            .view('BeautySalonViews/feedbackPage')
            . view('Common/footer');
    }

    public function feedbackSuccess(){
        return view('Common/common')
            . view('Common/header')
            . view('Common/menu')
            .view('BeautySalonViews/feedbacksuccess')
            . view('Common/footer');
    }

    public function userprofile(){
        return view('Common/common')
            . view('Common/header')
            . view('Common/menu')
            .view('BeautySalonViews/userprofile')
            . view('Common/footer');
    }

    public function adminprofile(){
        return view('Common/common')
            . view('Common/header')
            . view('Common/menu')
            .view('BeautySalonViews/adminprofile')
            . view('Common/footer');
    }

    public function index(){
        $data['common']= view ('Common/common');
        $data['header']= view('Common/header');
        $data['menu']= view('Common/menu');
        $data['footer']= view('Common/footer');
        return view('BeautySalonViews/homepage', $data);
    }

    public function productpage($id){
        $data['common']= view ('Common/common');
        $data['header']= view('Common/header');
        $data['menu']= view('Common/menu');
        $data['footer']= view('Common/footer');
        $data['id'] = $id;
        return view('BeautySalonViews/productpage', $data);
    }

    public function products(){
        $data['common']= view ('Common/common');
        $data['header']= view('Common/header');
        $data['menu']= view('Common/menu');
        $data['footer']= view('Common/footer');
        return view('BeautySalonViews/products', $data);
    }

    public function fetchProductByID($id){
        $product = new Product();
        $data['product'] = $product->getProductByID($id);
        return $this->response->setJSON($data);
    }

    public function fetchProductsByCategory($category){
        $products = new Product();
        $data['products'] = $products->getProductsByCategory($this->categoryArray[$category]);
        $data['category'] = $this->categoryArray[$category];
        return $this->response->setJSON($data);
    }

    public function fetchCartProducts(){
        $product = new Product();
        $data['cartProducts'] = [];
        if (!empty(session('products'))){
            foreach (session('products') as $cartProduct){
                
                $array = $product->getProductByID($cartProduct['id']);
                $array['cantidad']= $cartProduct['cantidad'];
                $array['total'] = floatval($array['cantidad']) * floatval($array['precio']) ;
                array_push($data['cartProducts'], $array);
            }
        }
        return $this->response->setJSON($data);
    }

    public function fetchUser(){
        $user = new UserInfo();
        if (!empty(session('user'))){
            $userArr['userArray'] = $user->getUserInfo(session('user'));
        }
        return $this->response->setJSON($userArr);
    }

    public function fetchAppointmentsByUser(){
        $appointment = new Appointment();
        if (!empty(session('user'))){
            $appointmentArr['appointments'] = $appointment->getAppointmentsByUser(session('user'));
        }
        return $this->response->setJSON($appointmentArr);
    }

    public function fetchAdminInfo(){
        $appointment = new Appointment();
        $feedback = new Feedback();
        if (!empty(session('user'))){
            $data['appointments'] = $appointment->getAllAppointments();
            $data['feedback'] = $feedback->getAllFeedback();
        }
        return $this->response->setJSON($data);
    }
}