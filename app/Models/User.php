<?php 
namespace App\Models;

use CodeIgniter\Model;

class User extends Model{
    protected $table      = 'usuarios';
    // Uncomment below if you want add primary key
    protected $primaryKey = 'usuario';
    protected $allowedFields = ['usuario', 'password', 'type'];

    public function getUser($data){
        return $this->where($data)->first();
    }

    public function insertUser($data){
        return $this->insert($data);
    }
}