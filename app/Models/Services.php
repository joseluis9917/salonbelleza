<?php 
namespace App\Models;

use CodeIgniter\Model;

class Services extends Model{
    protected $table      = 'servicios';
    // Uncomment below if you want add primary key
    protected $primaryKey = 'id_servicio';
    protected $allowedFields = ['id_servicio', 'nombre', 'precio', 'duracion', 'categoria'];

    public function getServiceByID($id){
        return $this->where('id_servicio', $id)->first();
    }

    public function getDurationByID($id){
        $this->select('duracion');
        return $this->where('id_servicio', $id)->first();
    }   

}