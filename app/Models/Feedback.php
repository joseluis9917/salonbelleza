<?php 
namespace App\Models;

use CodeIgniter\Model;

class Feedback extends Model{
    protected $table      = 'comentarios';
    // Uncomment below if you want add primary key
    protected $primaryKey = 'id_comentario';
    protected $allowedFields = ['id_comentario', 'nombre_contacto', 'asunto', 'correo_electronico', 'comentario'];

    public $validationRules = [
        'asunto' => [ 'rules' => 'required|max_length[20]', 'errors' => [ 'required' => 'asunto requerido', ], ],
        'nombre_contacto' => [ 'rules' => 'required|max_length[250]', 'errors' => [ 'required' => 'nombre requerido', ], ],
        'correo_electronico' => [ 'rules' => 'required|max_length[250]', 'errors' => [ 'required' => 'correo electronico requerido', ], ],
    ];

    public function insertFeedback($data){
        $this->insert($data);
    }

    public function getAllFeedback(){
        $this->select('*');
        $query = $this->get();
        return $query->getResultArray();
    }
}