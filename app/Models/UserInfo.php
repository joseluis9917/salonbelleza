<?php 
namespace App\Models;

use CodeIgniter\Model;

class UserInfo extends Model{
    protected $table      = 'informacion_usuarios';
    // Uncomment below if you want add primary key
    protected $primaryKey = 'usuario';
    protected $allowedFields = ['usuario', 'nombre', 'apellido_paterno', 'apellido_materno', 'alias', 'calle', 'numero_casa', 'colonia', 'codigo_postal', 'correo_electronico', 'numero_celular', 'numero_local'];

    public function insertUserInfo($data){
        return $this->insert($data);
    }

    public function getUserInfo($usuario){
        $this->select('*');
        return $this->where('usuario',$usuario)->first();
    }
}