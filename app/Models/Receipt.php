<?php 
namespace App\Models;

use CodeIgniter\Model;

class Receipt extends Model{
    protected $table      = 'ordenes';
    // Uncomment below if you want add primary key
    protected $primaryKey = 'id_ticket';
    protected $allowedFields = ['id_ticket', 'usuario', 'fecha', 'productos', 'total'];

    public function insertReceipt($data){
        return $this->insert($data);
    }
}