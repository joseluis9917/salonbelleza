<?php 
namespace App\Models;

use CodeIgniter\Model;

class Appointment extends Model{
    protected $table      = 'citas';
    // Uncomment below if you want add primary key
    protected $primaryKey = 'id_cita';
    protected $allowedFields = ['id_cita', 'id_servicio', 'usuario', 'fecha', 'hora_inicio', 'hora_fin'];

    public function insertAppointment($data){
        $this->insert($data);
    }

    public function getAppointmentsByDate($id_servicio, $fecha, $hora_inicio){
        $this->select('hora_inicio')
            ->select('hora_fin');
        $this->where('id_servicio', $id_servicio);
        $this->where('fecha', $fecha);
        $this->where('hora_inicio', $hora_inicio);
        $query = $this->get();
        return $query->getResultArray();
    }

    public function getAllAppointments(){
        $this->select('*');
        $query = $this->get();
        return $query->getResultArray();
    }

    public function getAppointmentsByUser($user){
        $this->select('*');
        $this->where('usuario', $user);
        $query = $this->get();
        return $query->getResultArray();
    }
}