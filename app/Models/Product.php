<?php 
namespace App\Models;

use CodeIgniter\Model;

class Product extends Model{
    protected $table      = 'productos';
    // Uncomment below if you want add primary key
    protected $primaryKey = 'id_producto';
    protected $allowedFields = ['id_producto', 'nombre', 'precio', 'id_categoria'];

    public function getProductByID($id){
        return $this->where('id_producto', $id)->first();
    }

    public function getProductsByCategory($category){
        $this->select('*');
        $this->where('id_categoria', $category);
        $query = $this->get();
        return $query->getResultArray();
    }
}