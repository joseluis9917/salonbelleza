<?=$common?>
<?=$header?>
<?=$menu?>
<style>
    .title{
        text-align: center;
        z-index: 3;
        top: 0px;
        padding: 10px;
        background-color: #e9ecef;
    }
</style>
<div class="container">
    <br>
    <h3 class="title">C A R R I TO</h3>
    <br>
    <?php
        if (session('products')==null && session('appointments')==null){
            echo '<div class="container">
                    <h3 style="text-align: center;">Su carrito se encuentra vacío.</h3>
                </div>
            ';
        }
    ?>
    
    <?php
        if (session('products')!=null){
            echo '<div class="container">
                <div class="row">
                    <div class="col-2 text-center">
                        Producto
                    </div>
                    <div class="col"></div>
                    <div class="col text-center">
                        Precio
                    </div>
                    <div class="col text-start">
                        Cantidad
                    </div>
                    <div class="col text-end" >
                        Total
                    </div>
                </div>
            </div>
            <div id="product-card" class="container">
            </div>
            ';
        }
    ?>
    
    
    <?php
        if (session('appointments')!=null){
            echo '<div class="container">
                <div class="row">
                    <div class="col-2 text-center">
                        Servicio
                    </div>
                    <div class="col"></div>
                    <div class="col text-center">
                        Fecha
                    </div>
                    <div class="col text-start">
                        Horario
                    </div>
                    <div class="col text-end" >
                        Total
                    </div>
                </div>
            </div>
            <div id="service-card" class="container">
            </div>
            ';
        }
    ?>
    
    
    <?php
        if (session('appointments')!=null || session('products')!=null){
            echo '<div id="total-info" class="container">
            </div>';
        }
    ?>
    
     
</div>

<script>
    var total = 0

    async function listProducts(){
        let url= 'http://localhost/salonBelleza/public/fetchCartProducts';
        //console.log(url);
        let obj;
        try{
            const res = await fetch(url);
            obj = await res.json();
            //console.log(obj);
            document.getElementById('product-card').innerHTML = drawProduct(obj);
            
        } catch (error){
            console.log(error);
        }
        document.getElementById('total-info').innerHTML = drawTotal();
    }

    async function listAppointments(){
        let url= 'http://localhost/salonBelleza/public/fetchAppointments';
        //console.log(url);
        let obj;
        try{
            const res = await fetch(url);
            obj = await res.json();
            console.log(obj);
            document.getElementById('service-card').innerHTML = drawAppointment(obj);
        } catch (error){
            console.log(error);
        }
    }

    function drawAppointment(obj){
        let appointment_info_inner = ''
        
        obj.appointments.forEach(function(appointment){
            total = total + parseFloat(appointment.precio) 
            appointment_info_inner += `
                <hr class="footer_page">
                    <div class="row">
                        <div class="col-2 text-center">
                            <img src="<?=base_url('img/services')?>/${appointment.foto}" width="100" height="100">
                        </div>
                        <div class="col"  style="align-self: center;">${appointment.nombre}<br>
                            <a href="<?=base_url('removeAppointment')?>/${appointment.id_servicio}" tabindex="0">Remove</a>
                        </div>
                        <div class="col text-center" style="align-self: center;">
                            ${appointment.fecha}
                        </div>
                        <div class="col text-start" style="align-self: center;">
                            ${appointment.hora_inicio} - ${appointment.hora_fin}
                        </div>
                        <div class="col text-end" style="align-self: center;">
                            $${appointment.precio}
                        </div>
                    </div>
                <hr class="footer_page">
            `
        })
        
        return appointment_info_inner
    }

    function drawProduct(obj){
        let product_info_inner = ''
        
        obj.cartProducts.forEach(function(product){
            total = total + parseFloat(product.total) 
            product_info_inner += `
                <hr class="footer_page">
                    <div class="row">
                        <div class="col-2 text-center">
                            <img src="<?=base_url('img/products')?>/${product.foto1}" width="100" height="100">
                        </div>
                        <div class="col"  style="align-self: center;">${product.nombre}<br>
                            <a href="<?=base_url('removeProduct')?>/${product.id_producto}" tabindex="0">Remove</a>
                        </div>
                        <div class="col text-center" style="align-self: center;">
                            $${product.precio}
                        </div>
                        <div class="col text-start" style="align-self: center;">
                            <input type="number" name="quantity-2" value="${product.cantidad}" min="1" max="10" placeholder="Quantity" required="" readonly=readonly>
                        </div>
                        <div class="col text-end" style="align-self: center;">
                            $${product.total}
                        </div>
                    </div>
                <hr class="footer_page">
            `
        })
        
        return product_info_inner
    }

    function drawTotal(){
        var total_descuento = <?=session('discount')?>+0
        console.log(total_descuento);
        total = total-(total*total_descuento)
        let aux = ''
        switch (total_descuento){
            case 0.1:
                aux = 'Con 10% de descuento'
                break
            case 0.2:
                aux = 'Con 20% de descuento'
                break
            case 0.3:
                aux = 'Con 30% de descuento'
                break;
        }
        
        let total_price = ''
        total_price += `
        <div class="container ml-auto">
            <div class="row">
                <div class="col"></div>
                <div class="col text-end">
                    Subtotal ${aux}
                </div>
                <div class="col-md-auto text-end">
                    $${total}
                </div>
            </div>  
            <br>
            <div class="row">
                <div class="col text-end">
                    <a href="<?=base_url('processOrder')?>/${total}" class="btn btn-primary">Realizar pedido</a>
                </div>
            </div>    
        </div>
        `
        return total_price;
    }

    listAppointments()
    listProducts();
</script>
<?=$footer?>