

<style>

    
    button{
        border: none;
        width: 100%;
        height: 40px;
        background-color: #a76671;
        font-family: Dubai Light, Century Gothic;
        color: #fff;
        margin: 0;
        font-size: 20 px;
    }

    button2{
        border: none;
        width: 100%;
        height: 30px;
        background-color: transparent;
        font-family: Dubai Light, Century Gothic;
        text-align: center;
        color: #a76671;
        margin: 0;
        font-size: 0 px;
    }
</style>

<!-- container-fluid-->
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-3 col-lg-4">
        </div>    
        <div class="col-12 col-sm-6 col-lg-4">
            <br>
            <h1 style="text-align: center; font-family: Dubai Light, Century Gothic;" >Inicio de sesión</h1>
            
            <p> <?php
            if (session('error')!=null){
                echo '<ul><li style="color: #d56262;">'.session('error').'</li></ul>';
            } 
            ?></p>
            <?=form_open('processLogin','class="needs-validation" novalidate');?>
                <div class="form-group">
                    <label style="font-family: Dubai Light, Century Gothic;" for="usuario" >Usuario</label>
                    <input type="text" class="form-control" placeholder="Usuario" id="usuario" name="usuario" required="">
                    <div class="invalid-feedback">
                        Usuario requerido.
                    </div>
                </div>
                <div class="form-group">
                    <label style="font-family: Dubai Light, Century Gothic;" for="password">Password</label>
                    <input type="password" class="form-control" placeholder="Password" id="password" name="password" required="">
                    <div class="invalid-feedback">
                        Contraseña requerida.
                    </div>
                </div>
                <div class="form">
                    <button type="submit">I N G R E S A R</button>
                </div>
            <?=form_close()?>
        </div>
    </div>
</div>    

<script type="application/javascript">
    (function () {
        'use strict'
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')
        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
          .forEach(function (form) {
            form.addEventListener('submit', function (event) {
              if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
              }

              form.classList.add('was-validated')
            }, false)
          })
    })();
</script>