<div class="container mt-3"> <!-- ---------------------CONTENEDOR PRINCIPAL DEL ACORDEÓN--------------------- -->

    <div class="row">
      <div class="col-sm-6">
        <h1><i class="fa-solid fa-user"></i> Perfil del Administrador</h1>
      </div>
    </div>
  
    <hr >

    <div class="accordion accordion-flush" id="accordionFlushExample">
      <div class="accordion-item">
        <h2 class="accordion-header">
          <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
            Citas
          </button>
        </h2>
        <div id="flush-collapseOne" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
          <div class="accordion-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Usuario</th>
                  <th scope="col">Servicio</th>
                  <th scope="col">Fecha</th>
                  <th scope="col">Hora de inicio</th>
                  <th scope="col">Hora de fin</th>
                </tr>
              </thead>
              <tbody id="appointments-body">
                <tr>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="accordion-item">
        <h2 class="accordion-header">
          <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
            Comentarios
          </button>
        </h2>
        <div id="flush-collapseTwo" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
          <div class="accordion-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">ID Comentario</th>
                  <th scope="col">Nombre del Contacto</th>
                  <th scope="col">Asunto</th>
                  <th scope="col">Correo Electronico</th>
                  <th scope="col">Comentario</th>
                </tr>
              </thead>
              <tbody id="feedback-body">
                <tr>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

  </div>

  <script>
    async function listInfo(){
        let url= 'http://localhost/salonBelleza/public/fetchAdminInfo';
        console.log(url);
        let obj;
        try{
            const res = await fetch(url);
            obj = await res.json();
            console.log(obj);
            document.getElementById('appointments-body').innerHTML = drawAppointments(obj);
            document.getElementById('feedback-body').innerHTML = drawFeedback(obj);
        } catch (error){
            console.log(error);
        }
    }

    function drawFeedback(obj){
        let aux = ''
        
        obj.feedback.forEach(function(comment){
            aux += `
            <tr>
                <td>${comment.id_comentario}</td>
                <td>${comment.nombre_contacto}</td>
                <td>${comment.asunto}</td>
                <td>${comment.correo_electronico}</td>
                <td>${comment.comentario}</td>
            </tr>
        `
        })
        
        return aux
    }

    function drawAppointments(obj){
        let aux = ''
        
        obj.appointments.forEach(function(appointment){
            aux += `
            <tr>
                <td>${appointment.id_cita}</td>
                <td>${appointment.usuario}</td>
                <td>${appointment.id_servicio}</td>
                <td>${appointment.fecha}</td>
                <td>${appointment.hora_inicio}</td>
                <td>${appointment.hora_fin}</td>
            </tr>
        `
        })
        
        return aux
    }

    listInfo()
</script>