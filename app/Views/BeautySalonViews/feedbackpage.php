
<div class="homepage-divider"></div>
<div class="container">

    <!--Section heading-->
    <h2 class="h1-responsive font-weight-bold text-center my-4">Comentarios</h2>
    <!--Section description-->
    <p class="text-center w-responsive mx-auto mb-5">¿Tienes alguna duda o comentario? No dudes en contactarnos directamente. Nuestro equipo
        se encargará de ayudarte.
    </p>

    <div class="row">

        <!--Grid column-->
        <div class="col-md-9 mb-md-0 mb-5">
            <?=form_open('proccessFeedback','class="needs-validation" novalidate');?>
                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0 form-group">
                            <label for="nombre_contacto" class="form-label">Nombre de contacto</label>
                            <input type="text" id="nombre_contacto" name="nombre_contacto" class="form-control" required="">
                            <div class="invalid-feedback">
                                Un nombre de contacto es requerido.
                            </div>
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0 form-group">
                            <label for="correo_electronico" class="">Correo electrónico</label>
                            <input type="email" id="correo_electronico" name="correo_electronico" class="form-control" required="">
                            <div class="invalid-feedback">
                                Un correo electronico es requerido.
                            </div>
                        </div>
                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-form mb-0 form-group">
                            <label for="asunto" class="">Asunto</label>
                            <input type="text" id="asunto" name="asunto" class="form-control" required="">
                            <div class="invalid-feedback">
                                Un asunto es requerido.
                            </div>
                        </div>
                    </div>
                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-12">

                        <div class="md-form form-group">
                            <label for="comentario">Mensaje</label>
                            <textarea type="text" id="comentario" name="comentario" rows="2" class="form-control md-textarea"></textarea>                            
                        </div>

                    </div>
                </div>
                <!--Grid row-->
                <p></p>
                <div class="text-center text-md-left">
                    <button class="btn btn-primary" type="submit">Enviar</button>
                </div>
            <?=form_close()?>

            
            <div class="status"></div>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-3 text-center">
            <ul class="list-unstyled mb-0">
                <li><i class="fas fa-map-marker-alt fa-2x"></i>
                    <p>Puebla, PUE. 72000, MEX.</p>
                </li>

                <li><i class="fas fa-phone mt-4 fa-2x"></i>
                    <p>+ 222 533 8943</p>
                </li>

                <li><i class="fas fa-envelope mt-4 fa-2x"></i>
                    <p>contact@bcsalon.com</p>
                </li>
            </ul>
        </div>
        <!--Grid column-->

    </div>
    <br><br><br><br>
</div>
<script type="application/javascript">
    (function () {
        'use strict'
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')
        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
          .forEach(function (form) {
            form.addEventListener('submit', function (event) {
              if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
              }

              form.classList.add('was-validated')
            }, false)
          })
    })();
</script>
<div class="homepage-divider"></div>
