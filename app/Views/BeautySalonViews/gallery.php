
<style>
* {
	box-sizing: border-box;
}

b {
	color: #a26d85;
    font-family: Dubai Light, Century Gothic;
	min-height: 100vh;
	display: flex;
	align-items: center;
}

.contenedor {
	max-width: 1200px;
	width: 90%;
	margin: auto;
}

.contenido-principal {
	margin-bottom: 50px;
	display: flex;
	align-items: center;
}

.contenido-principal__imagen {
	vertical-align: top;
	margin-right: 20px;
	width: 50%;
	max-width: 550px;
}

.contenido-principal__contenedor {
	width: 50%;
}

.contenido-principal__titulo {
	font-weight: normal;
	font-size: 40px;
}

.contenido-principal__resumen {
	font-family: Dubai Light, Century Gothic;
	text-align: justify;
	font-size: 18px;
	line-height: 35px;
	color: #a26d85;
}

.img-container {
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    float: left;
    width: 33.33%;
    padding: 5px;
}
.img-container img{
    width: 100%;
}

.carousel__contenedor {
	position: center;
}

.carousel__anterior,
.carousel__siguiente {
    z-index: 3;
	position: absolute;
	display: block;
	width: 30px;
	height: 50px;
	border: none;
	top: calc(50% - 35px);
	cursor: pointer;
	line-height: 30px;
	text-align: center;
	background: none;
	color: #a26d85;
	opacity: 70%;
}

.carousel__anterior:hover,
.carousel__siguiente:hover {
	opacity: 100%;
}

.carousel__anterior {
	left: -40px;
}

.carousel__siguiente {
	right: -40px;
}

.carousel__lista {
	overflow: hidden;
}

.carousel__elemento {
	text-align: center;
}

.carousel__indicadores .glider-dot {
	display: block;
	width: 50px;
	height: 4px;
	background: #e2bacb;
	opacity: 50;
	border-radius: 0;
}

.carousel__indicadores .glider-dot:hover {
	opacity: .5;
}

.carousel__indicadores .glider-dot.active {
	opacity: 1;
}

@media screen and (max-width: 800px) {
	b {
		padding: 40px 0;
	}

	.contenido-principal {
		flex-direction: column;
	}

	.contenido-principal > * {
		width: 100%;
	}
}

</style>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Carousel / Slider con Glider.js</title>
	<link href="https://fonts.googleapis.com/css2?family=Montserrat&family=Open+Sans&display=swap" rel="stylesheet"> 
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.1/normalize.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/glider-js@1.7.3/glider.min.css">
	<link rel="stylesheet" href="css/estilos.css">
</head>
<b>
	<div class="contenedor">
		<main class="contenido-principal">
            <p> </p>
			<img src="http://localhost/salonBelleza/public/img/gallery/salon.jpg" alt="Dome of the German Bundestag" class="contenido-principal__imagen">
			<div class="contenido-principal__contenedor">
				<h1 class="contenido-principal__titulo">Galeria</h1>
				<p class="contenido-principal__resumen">
				En BeautyCare nos dedicamos a cuidar de ti, te ofrecemos los mejores servicios para tus uñas, cabello y rostro, a continuación te presentamos algunos de nuestros trabajos realizados en nuestras instalaciones de tal manera que así puedas conocer mejor lo que hacemos nuestro salón.  
				</p>
				<p class="contenido-principal__resumen">
					Hecho con amor por BeautyCare...


				</p>
                <div class="img-container">
                <img src="http://localhost/salonBelleza/public/img/gallery/nails0.jpg">
                </div>
                <div class="img-container">
                <img src="http://localhost/salonBelleza/public/img/gallery/hair0.jpg">
                </div>
                <div class="img-container">
                <img src="http://localhost/salonBelleza/public/img/gallery/face0.jpg">
                </div>
			</div>
		</main>

		<div class="carousel">
			<div class="carousel__contenedor">
				<button aria-label="Anterior" class="carousel__anterior">
					<i class="fas fa-chevron-left"></i>
				</button>
				<div class="carousel__lista">
					<div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/nails1.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
					<div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/nails2.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/nails3.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/nails4.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/nails5.png" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/nails6.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/nails7.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/nails8.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/nails9.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/nails10.png" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/nails11.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/nails12.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/nails13.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/nails14.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/nails15.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/nails16.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
					<div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/hair1.jpeg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/hair2.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/hair3.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/hair4.jpeg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/hair5.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/hair6.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/hair7.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
					<div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/hair8.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/hair9.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/hair10.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
					<div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/hair11.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/hair12.jpeg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
					<div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/face1.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/face2.jpeg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/face3.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/face4.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/face5.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/face6.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
                    <div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/face7.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
					<div class="carousel__elemento">
						<img src="http://localhost/salonBelleza/public/img/gallery/face8.jpg" alt="Rock and Roll Hall of Fame">
						<p> </p>
					</div>
				</div>
				<button aria-label="Siguiente" class="carousel__siguiente">
					<i class="fas fa-chevron-right"></i>
				</button>
			</div>
			<div role="tablist" class="carousel__indicadores"></div>
		</div>
	</div>

	<script src="https://cdn.jsdelivr.net/npm/glider-js@1.7.3/glider.min.js"></script>
	<script src="https://kit.fontawesome.com/2c36e9b7b1.js" crossorigin="anonymous"></script>
	<script src="js/app.js"></script>
</b>

</html>

<script src="js/jquery-3.6.0.min.js"></script>  
<script src="bootstrap.bundle.min.js"></script>  
<script src="js/sidebars.js"></script>

<script>

window.addEventListener('load', function(){
	new Glider(document.querySelector('.carousel__lista'), {
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: '.carousel__indicadores',
		arrows: {
			prev: '.carousel__anterior',
			next: '.carousel__siguiente'
		},
		responsive: [
			{
			  // screens greater than >= 775px
			breakpoint: 450,
			settings: {
				// Set to `auto` and provide item width to adjust to viewport
				slidesToShow: 2,
				slidesToScroll: 2
			}
			},{
			  // screens greater than >= 1024px
			breakpoint: 800,
			settings: {
				slidesToShow: 4,
				slidesToScroll: 4
			}
			}
		]
	});
});

</script>
