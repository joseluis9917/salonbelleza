<?=$common?>
<?=$header?>
<?=$menu?>

<div id="homepageCarousel" class="carousel slide" data-bs-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="<?=base_url('img/banner.jpg')?>" class="d-block w-100 banner_main" alt="banner">
      <div class="container">
        <div class="carousel-caption text-left">
            <h1>Nuevos productos</h1>
            <p> Ingrese a nuestro acogedor espacio, donde cada estante está adornado con una exquisita selección de productos cosméticos, para el cuidado de la piel, para el cabello y fragancias de marcas reconocidas y artesanos emergentes por igual.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container px-4 py-5">
    <h2 class="pb-2 border-bottom">Categorias</h2>
    <div class="row g-4 py-5 row-cols-1 row-cols-lg-3">
        <div class="feature col">
            <div class="feature-icon d-inline-flex align-items-center justify-content-center text-bg-primary bg-gradient fs-2 mb-3">
              <i class="fa-solid fa-images"></i>
            </div>
            <h3 class="fs-2" style="font-weight: 506 !important;">Galeria</h3>
            <p>Esta exquisita colección de imágenes captura la esencia del ambiente, los servicios y las experiencias transformadoras de nuestro salón que creamos para nuestros queridos clientes.</p>
        </div>
        <div class="feature col">
            <div class="feature-icon d-inline-flex align-items-center justify-content-center text-bg-primary bg-gradient fs-2 mb-3">
              <i class="fa-solid fa-bag-shopping"></i>
            </div>
            <h3 class="fs-2" style="font-weight: 506 !important;">Productos</h3>
            <p>Aquí, descubrirá una colección meticulosamente seleccionada de productos de belleza de primera calidad, cuidadosamente seleccionados para satisfacer las diversas necesidades y deseos de nuestra exigente clientela.</p>
        </div>
        <div class="feature col">
            <div class="feature-icon d-inline-flex align-items-center justify-content-center text-bg-primary bg-gradient fs-2 mb-3">
              <i class="fa-solid fa-scissors"></i>
            </div>
            <h3 class="fs-2" style="font-weight: 506 !important;">Servicios</h3>
            <p>Nuestro equipo altamente calificado y apasionado de profesionales de la belleza se dedica a brindarle una experiencia inolvidable en el salón. Con una gran cantidad de conocimientos y experiencia, están comprometidos a comprender sus deseos únicos y adaptar cada servicio para satisfacer sus necesidades específicas.</p>
        </div>
    </div>
</div>

<div class="container">
  <div class="card">
    <div class="row">
      <div class="col">
        <img src="<?=base_url('img/salon.jpg')?>" class="img-fluid rounded-start banner_card" style="object-fit: cover; object-position: center center; display: block; position: absolute; top: 0; left: 0; width: 100%; max-width: 100%; height: 100%;" alt="...">
      </div>
      <div class="col">
        <div class="card-body">
          <h5 class="card-homepage-title">Nosotros</h5>
          <p class="card-homepage-text">Bienvenido a BeautyCare Salón, su paraíso para la belleza, la relajación y el rejuvenecimiento. Ubicado en el corazón de Puebla, nuestro exclusivo salón de belleza ofrece un exquisito escape de las exigencias de la vida cotidiana. Estamos dedicados a brindar una experiencia placentera que satisfaga sus necesidades únicas, asegurándonos de que se vaya sintiéndose radiante, confiado y completamente transformado.</p>
          <br><br><br><br><br>
          <p class="card-homepage-text"><small class="text-muted">Last updated 5 months ago</small></p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="homepage-divider-1"></div>

<div class="container">
  <div class="row">
    <div class="col">
      <div class="card">
        <img class="card-img-top" src="<?=base_url('img/blog1.jpg')?>" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 350px;" alt="Card image cap">
        <div class="card-body">
          <h1 class="card-homepage-title">Artículos de belleza que necesitas en tu vida.</h1>
          <p class="card-homepage-text">Algunos te harán la vida más fácil, otros más divertida.</p>
          <a href="#">Leer artículo &#8594;</a>
          <p></p>
          <p class="card-homepage-text"><small class="text-muted">Last updated 3 mins ago</small></p>
        </div>
      </div>
    </div>
    <div class="col">
      <div class="card">
        <img class="card-img-top" src="<?=base_url('img/blog2.jpg')?>" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 350px;" alt="Card image cap">
        <div class="card-body">
          <h1 class="card-homepage-title">¿Qué dice tu pelo sobre ti?</h1>
          <p class="card-homepage-text">¿Te pasas un buen rato peinándote o solo lo lavas y cruzas los dedos?</p>
          <a href="#">Leer artículo &#8594;</a>
          <p></p>
          <p class="card-homepage-text"><small class="text-muted">Last updated 3 mins ago</small></p>
        </div>
      </div>
    </div>
    <div class="col">
      <div class="card">
        <img class="card-img-top" src="<?=base_url('img/blog3.jpg')?>" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 350px;" alt="Card image cap">
        <div class="card-body" >
          <h1 class="card-homepage-title">Las mejores marcarillas faciales skincare.</h1>
          <p class="card-homepage-text">Francesas, coreanas y toda la cosmética del mundo a la orden de tu cara.</p>
          <a href="#">Leer artículo &#8594;</a>
          <p></p>
          <p class="card-homepage-text"><small class="text-muted">Last updated 3 mins ago</small></p>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="homepage-divider"></div>
<!--</div>-->



<?=$footer?>