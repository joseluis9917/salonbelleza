

<style>

    @media only screen and (min-width: 80em) {

        b{
            
            width: 70%;
            max-width: 80rem;
            margin: 0;
            padding: 65px;
            box-sizing: border-box;
            display: flex;
            align-items: center;
            height: 100vh;
            flex-direction: column;
            font-family: Dubai Light, Century Gothic;
        }
        g{  
            position: absolute; left: 550px; top: 1px;
            width: 30%;
            max-width: 80rem;
            margin: 0;
            padding: 50px;
            box-sizing: border-box;
            display: inline block;
            align-items: center;
            height: 100vh;
            flex-direction: column;
            font-family: Dubai Light, Century Gothic;
        }
    }       

    .date-picker {
        font-size: 1.2rem;
    }

    #picker {
        font-weight: bold;
    }

    td {
        cursor: pointer;
        text-align: center;
    }
    td:hover {
        background-color: rgb(237, 237, 237);
    }

    main{
        background-color: #fff;
        border-radius: 10px; 
        width: 300px;
        height: 260px;
        border: 2px solid #ca9ba3;
        display: flex;
        align-items: center;
        flex-direction: column;
        overflow: hidden;
    }

    #calender-header{
        display: flex;
        justify-content: space-between;
        background-color: #ca9ba3;
        width: 300px;
        color: #fff;
    }

    button{
        font-family: cursive, "Comic Sans", Comic;
        border: none;
        background-color: transparent;
        color: transparent;
        margin: 0 20px;
    }

    .today{
        background-color: #a76671;
        color: #fff;
    }

    s {
        display: flex;
        align-items: center;
        justify-content: center;
        margin: 0 auto;
        height: 100vh; 
        background-color: #f1f1f1;
        font-family: Dubai Light, Century Gothic;
    }

    input {
        display: inline;
        align-items: relative;
        justify-content: relative;
        margin: 0 auto;
        font-family: Dubai Light, Century Gothic;
        color: #985C66;
    }

    select {
        margin-bottom: 10px;
        margin-top: 10px;
        font-family: Dubai Light, Century Gothic;
        color: #985C66;
        background-color: #F7F0F2;
    }

</style>

    <b>
        <div >
        <button onclick="prevMonth()"><</button>
        
        <button onclick="nextMonth()">></button>
        </div>
        <table>
        <p> </p>
        <thead>
        </thead>
        <tbody id="calendar">
        </tbody>
        </table>
    </main>
    </b>
    <g>
        <body>
            <h4 class="datepicker" style="color: #BE9098;"><span class="date-formate" id="picker"> 23 - july - 2022</span></h4>
            <h1 class="time-formate" style="color: #a76671;">02:51:20</h1>
            <?php 
            echo $servicio['nombre'].'<br>';
            echo $servicio['duracion'].' mins.';
            ?>
            <p>  </p>
        </body>
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
        <script src="jquery.ui.datepicker-es.js"></script>
        <script>
            $(function() {
                $("#datepicker").datepicker({ minDate: 0, maxDate: "+4M, -10D" });
            });
            jQuery(function($){
                $.datepicker.regional['es'] = {
                    monthNames: ['enero','febrero','marzo','abril','mayo','junio',
                    'julio','agosto','septiembre','octubre','noviembre','diciembre'],
                    dayNamesMin: ['D','L','M','X','J','V','S'],
                    dateFormat: 'mm/dd/yy',
                    isRTL: false,
                    showMonthAfterYear: false,
                    yearSuffix: ''};
                $.datepicker.setDefaults($.datepicker.regional['es']);
            });
        </script>
        <?=form_open('proccessDate/'.$servicio['id_servicio']);?>
            <p>Fecha de cita: <input type="text" id="datepicker" required="" name="fecha">
                <input type="submit" value="Enviar"/>
            </p>
        <?=form_close()?>
    </g>

<!--<script src="js/jquery-3.6.0.min.js"></script>  -->
<!--<script src="bootstrap.bundle.min.js"></script>  -->
<!--<script src="js/sidebars.js"></script>-->

<script>

const picker = document.getElementById("picker");
const month = document.getElementById("month");
const calendar = document.getElementById("calendar");

const DATE = new Date();
let thisMonth = DATE.getMonth();
let year = DATE.getFullYear();

const MONTHS = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre",
    ];

    picker.innerHTML = `${DATE.getDate()}/${thisMonth + 1}/${year}`;

    const createCalendar = () => {
    //month.innerHTML = `${MONTHS[thisMonth]}, ${year}`;

    const dayOne = new Date(year, thisMonth).getDay();
    const monthDays = 32 - new Date(year, thisMonth, 32).getDate();

    date = 1;
	
    for (let i = 0; i < 6; i++) {
        let row = document.createElement("tr");
        for (let j = 0; j < 7; j++) {
        let column = document.createElement("td");
        if (date > monthDays) break;
        else if (i === 0 && j < dayOne) {
            let columnText = document.createTextNode("");
            column.appendChild(columnText);
            row.appendChild(column);
        } else {
            let columnText = document.createTextNode(date);
            column.appendChild(columnText);

            if(date === DATE.getDate() && thisMonth === DATE.getMonth() && year === DATE.getFullYear()){
            column.classList.add("today")
            }

            column.onclick = () => {
            picker.innerHTML = `${column.textContent}/${thisMonth + 1}/${year}`;
            };


            date++;
        }
        }
    }
    };

    createCalendar();

    const nextMonth = () => {
    thisMonth = thisMonth + 1;
    calendar.innerHTML = ""

    if(thisMonth > 11){
        year = year + 1
        thisMonth = 0
    }
    createCalendar()
    return thisMonth
    };

    const prevMonth = () => {
    thisMonth = thisMonth - 1;
    calendar.innerHTML = ""

    if(thisMonth < 0){
        year = year - 1
        thisMonth = 11
    }
    createCalendar()
    return thisMonth
    };

    const todayShowTime = document.querySelector('.time-formate');
    const todayShowDate = document.querySelector('.date-formate');
    
    const currshowDate = new Date();
    const showCurrentDateOption = {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        weekday: 'long',
    };
    const currentDateFormate = new Intl.DateTimeFormat(
        'es-ES',
        showCurrentDateOption
    ).format(currshowDate);
    todayShowDate.textContent = currentDateFormate;
    setInterval(() => {
        const timer = new Date();
        const option = {
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric',
        };
        const formateTimer = new Intl.DateTimeFormat('es-es', option).format(timer);
        let time = `${`${timer.getHours()}`.padStart(
        2,
        '0'
        )}:${`${timer.getMinutes()}`.padStart(
        2,
        '0'
        )}: ${`${timer.getSeconds()}`.padStart(2, '0')}`;
        todayShowTime.textContent = formateTimer;
    }, 1000);
    

</script>