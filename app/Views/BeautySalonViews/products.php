<?=$common?>
<?=$header?>
<?=$menu?>

<div class="container-fluid" style="width: 95%;">
    <div class="homepage-divider-1"></div>
    <nav aria-label="breadcrumb" style="background-color: #f4f6f9;">
        <ol class="breadcrumb"> 
            <li class="breadcrumb-item"><a href="<?=base_url('homepage')?>" style="color: #3d3b3b;">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="<?=base_url('products')?>" style="color: #3d3b3b;">Productos</a></li>
        </ol> 
    </nav>
    <div id="productBannerCarousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner" style="height: 244px;">
            <div class="carousel-item active">
            <img src="<?=base_url('img/products_banner.jpg')?>" class="product_banner d-block w-100" alt="banner">
            </div>
        </div>
    </div>
    <div class="homepage-divider-1"></div>
    <h3 class="mb-1" id="category-title1"></h3>
    <div class="container-fluid" id="product-container1"></div>
    <h3 class="mb-1" id="category-title2"></h3>
    <div class="container-fluid" id="product-container2"></div>
    <h3 class="mb-1" id="category-title3"></h3>
    <div class="container-fluid" id="product-container3"></div>
    <h3 class="mb-1" id="category-title4"></h3>
    <div class="container-fluid" id="product-container4"></div>
    <h3 class="mb-1" id="category-title5"></h3>
    <div class="container-fluid" id="product-container5"></div>
    
<script>
    async function listProducts(category){
        let url= 'fetchProducts/'+category;
        console.log(url);
        let obj;
        try{
            const res = await fetch(url);
            obj = await res.json();
            console.log(obj);
            document.getElementById('category-title'+category).innerHTML = obj.category;
            document.getElementById('product-container'+category).innerHTML = drawProduct(obj);
        } catch (error){
            console.log(error);
        }
    }

    function drawProduct(obj){
        let product_container_inner = ''
        aux = 0

        obj.products.forEach(function(product){
            if (aux==0){
                product_container_inner += `<div class="row justify-content-md-center">`
            }
            product_container_inner += `
                <div class="col-md-auto">
                    <div class="card" style="width: 18rem;">
                        <img class="d-block w-100 rounded border" src="<?=base_url('img/products')?>/${product.foto1}">
                        <div class="card-body">
                            <span class="text-small mb-1">
                                <small>${product.id_categoria}</small>  
                            </span>
                            <h2 class="fs-6">
                                <a href="<?=base_url('productpage')?>/${product.id_producto}" class="text-inherit text-decoration-none" tabindex="-1">${product.nombre}</a>
                            </h2>
                            <div class="text-warning">
                                <small> 
                                    <i class="fa-solid fa-star"></i>
                                    <i class="fa-solid fa-star"></i>
                                    <i class="fa-solid fa-star"></i>
                                    <i class="fa-solid fa-star"></i>
                                    <i class="fa-solid fa-star-half-stroke"></i>
                                </small> 
                                <span class="text-muted small">4.5 (67)</span>
                            </div>
                            <div class="d-flex justify-content-between align-items-center mt-3">
                                <form method="post" action="<?=base_url('addProduct')?>/${product.id_producto}">
                                    <div>
                                        
                                        <select name="cantidad">
                                            <optgroup label="Cantidad">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                            </optgroup>
                                        </select>
                                        <br>
                                        <span class="text-dark">$${product.precio}</span>
                                        <br>
                                    </div>
                                    <div>
                                        <!--<a href="<?=base_url('addProduct')?>/${product.id_producto}" class="btn btn-primary btn-sm" tabindex="0"><i class="fa-solid fa-plus"></i>  Add</a>-->
                                        <button class="btn btn-primary btn-sm" type="submit"><i class="fa-solid fa-plus"></i>  Add</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            `
            aux++
            if (aux==4){
                    aux=0
                    product_container_inner += `</div>`
            }
            
        })
        if (aux!=4 && aux!=0){
            product_container_inner += `</div>`
        }
        return product_container_inner
    }

    listProducts('1');
    listProducts('2');
    listProducts('3');
    listProducts('4');
    listProducts('5');
</script>

    <section class="my-lg-14 my-8">
        <div class="container-fluid" style="width: 85%;">
            <hr class="footer_page">
            <!-- row -->
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="mb-8 mb-lg-0">
                        <!-- img -->
                        <div class="feature-icon d-inline-flex align-items-center justify-content-center text-bg-primary bg-gradient fs-2 mb-3">
                            <i class="fa-solid fa-truck-fast"></i>
                        </div>
                        <!-- title -->
                        <h3 class="mb-3">
                        Envios al día siguiente
                        </h3>
                        <!-- text -->
                        <p class="mb-0">Con nuestro sistema de envio recibe tus productos al día siguiente si tu domicilio se encuentra en la zona conurbada.</p>
                    </div>
                </div>
                <!-- col -->
                <div class="col-md-6  col-lg-3">
                    <div class="mb-8 mb-lg-0">
                        <!-- img -->
                        <div class="feature-icon d-inline-flex align-items-center justify-content-center text-bg-primary bg-gradient fs-2 mb-3">
                            <i class="fa-solid fa-tag"></i>
                        </div>
                        <!-- title -->
                        <h3 class="mb-3">Los mejores precios &amp; Ofertas</h3>
                        <!-- text -->
                        <p class="mb-0">Constantemente actualizamos nuestro catálogo de productos, así que las ofertas no se hacen esperar.
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="mb-8 mb-lg-0">
                        <!-- img -->
                        <div class="feature-icon d-inline-flex align-items-center justify-content-center text-bg-primary bg-gradient fs-2 mb-3">
                            <i class="fa-solid fa-arrows-rotate"></i>
                        </div>
                        <!-- title -->
                        <h3 class="mb-3">Gran variedad de productos</h3>
                        <!-- text -->
                        <p class="mb-0">Tenemos los mejores proveedores llevandote hasta tu casa los mejores productos de vanguardia.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="mb-8 mb-lg-0">
                        <!-- img -->
                        <div class="feature-icon d-inline-flex align-items-center justify-content-center text-bg-primary bg-gradient fs-2 mb-3">
                            <i class="fa-solid fa-gift"></i>
                        </div>
                        <!-- title -->
                        <h3 class="mb-3">Devoluciones sencillas</h3>
                        <!-- text -->
                        <p class="mb-0">¿No estás satisfecho con tu producto? Devuelvelo desde tu domicilio y obtén un reembolso en horas. No hacemos preguntas.
                    </div>
                </div>
            </div>
        </div>
  </section>
</div>
<br><br><br>
<div class="homepage-divider"></div>


<?=$footer?>