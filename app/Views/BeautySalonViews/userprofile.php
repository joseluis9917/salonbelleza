<div class="container mt-3"> <!-- ---------------------CONTENEDOR PRINCIPAL DEL ACORDEÓN--------------------- -->

    <div class="row">
      <div class="col-sm-5">
        <h1><i class="fa-solid fa-user"></i> Perfil del Usuario</h1>
      </div>
    </div>
    
    <hr >

    <div class="accordion accordion-flush" id="accordionFlushExample">
      <div class="accordion-item" id="user-info">
      </div>
      <div class="accordion-item" id="address-info">
      </div>

      <div class="accordion-item" id="">
        
      </div>
      <div class="accordion-item">
        <h2 class="accordion-header">
          <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
            Citas
          </button>
        </h2>
        <div id="flush-collapseFour" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
          <div class="accordion-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Servicio</th>
                  <th scope="col">Fecha</th>
                  <th scope="col">Hora de inicio</th>
                  <th scope="col">Hora de fin</th>
                </tr>
              </thead>
              <tbody id="appointments-body">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

  </div>
  <script>
    async function listUserInfo(){
        let url= 'http://localhost/salonBelleza/public/fetchUser';
        console.log(url);
        let obj;
        try{
            const res = await fetch(url);
            obj = await res.json();
            console.log(obj);
            document.getElementById('user-info').innerHTML = drawPersonalData(obj);
            document.getElementById('address-info').innerHTML = drawPersonalAddress(obj);
        } catch (error){
            console.log(error);
        }
    }

    function drawPersonalData(obj){
        let aux = ''
        user = obj.userArray

        aux += `
        <h2 class="accordion-header">
          <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
            Datos Personales
          </button>
        </h2>
        <div id="flush-collapseOne" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
          <div class="accordion-body">
            <form class="TestContenedor mb-3" action="" method="">
              <ul class="list-group">
                <li class="list-group-item" id="Title"><i class="fa-solid fa-user"></i> Datos Personales</li><!-- --------DATOS PERSONALES-------- -->
                <li class="list-group-item" id="Cont">

                  <div class="row mb-3">
                    <div class="col">
                      <div class="form-floating">
                        <input value="${user.usuario}" type="text" class="form-control" id="user" placeholder="Usuario" readonly=readonly>
                        <label for="user">Usuario</label>
                      </div>
                    </div>
      
                    <div class="col">
                      <div class="form-floating">
                        <input value="${user.nombre}" type="text" class="form-control" id="name" placeholder="Nombre" readonly=readonly>
                        <label for="name">Nombre</label>
                      </div>
                    </div>
                      
                  </div>
      
                  <div class="row mb-3">
      
                    <div class="col">
                      <div class="form-floating">
                        <input value="${user.apellido_paterno}" type="text" class="form-control" id="ap_pa" placeholder="Apellido Paterno" readonly=readonly>
                        <label for="ap_pa">Apellido Paterno</label>
                      </div>
                    </div>
      
                    <div class="col">
                      <div class="form-floating">
                        <input value="${user.apellido_materno}" type="text" class="form-control" id="ap_ma" placeholder="Apellido Materno" readonly=readonly>
                        <label for="ap_ma">Apellido Materno</label>
                      </div>
                    </div>
      
                  </div>
      
                  <div class="row mb-3">
      
                    <div class="col">
                      <div class="form-floating">
                        <input value="${user.correo_electronico}" type="email" class="form-control" id="correo_e" placeholder="Correo Electronico" readonly=readonly>
                        <label for="correo_e">Correo Electronico</label>
                      </div>
                    </div>
      
                    <div class="col">
                      <div class="form-floating">
                        <input value="${user.numero_celular}" readonly=readonly type="text" class="form-control" id="num_cel" placeholder="Número de Celular" >
                        <label for="num_cel">Número de Celular</label>
                      </div>
                    </div>
                  </div>

                </li>
              </ul>
            </form>
          </div>
        </div>
        `
        return aux
    }

    function drawPersonalAddress(obj){
        let aux = ''
        user = obj.userArray

        aux += `
        <h2 class="accordion-header">
          <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
            Dirección
          </button>
        </h2>
        <div id="flush-collapseTwo" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
          <div class="accordion-body">
            <form class="TestContenedor2 mb-3" action="" method="">
              <ul class="list-group">
                <li class="list-group-item" id="Title"><i class="fa-solid fa-address-book"></i> Dirección</li>
                <li class="list-group-item" id="Cont">
                    
                  <div class="row mb-3">
                    <div class="col">
                      <div class="form-floating">
                        <input value="${user.calle}" readonly=readonly type="text" class="form-control" id="street" placeholder="Calle">
                        <label for="street">Calle</label>
                      </div>
                    </div>
      
                    <div class="col">
                      <div class="form-floating">
                        <input value="${user.numero_casa}" readonly=readonly type="text" class="form-control" id="housenumber" placeholder="No. de Casa">
                        <label for="housenumber">No. de Casa</label>
                      </div>
                    </div>
                      
                  </div>
      
                  <div class="row mb-3">
      
                    <div class="col">
                      <div class="form-floating">
                        <input value="${user.colonia}" readonly=readonly type="text" class="form-control" id="colna" placeholder="Colonia">
                        <label for="colna">Colonia</label>
                      </div>
                    </div>
      
                    <div class="col">
                      <div class="form-floating">
                        <input value="${user.codigo_postal}" readonly=readonly type="text" class="form-control" id="zipcode" placeholder="Código Postal">
                        <label for="zipcode">Código Postal</label>
                      </div>
                    </div>
      
                  </div>
      
                  <div class="row mb-3">
      
                    <div class="col">
                      <div class="form-floating">
                        <input value="${user.numero_local}" readonly=readonly type="text" class="form-control" id="localnumber" placeholder="Número Local">
                        <label for="localnumber">Número Local</label>
                      </div>
                    </div>
                  </div>
      
                </li>
              </ul>
            </form>
          </div>
        </div>
        `
        return aux
    }

    function drawPersonalAddress(obj){
        let aux = ''
        user = obj.userArray

        aux += `
        <h2 class="accordion-header">
          <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
            Dirección
          </button>
        </h2>
        <div id="flush-collapseTwo" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
          <div class="accordion-body">
            <form class="TestContenedor2 mb-3" action="" method="">
              <ul class="list-group">
                <li class="list-group-item" id="Title"><i class="fa-solid fa-address-book"></i> Dirección</li>
                <li class="list-group-item" id="Cont">
                    
                  <div class="row mb-3">
                    <div class="col">
                      <div class="form-floating">
                        <input value="${user.calle}" readonly=readonly type="text" class="form-control" id="street" placeholder="Calle">
                        <label for="street">Calle</label>
                      </div>
                    </div>
      
                    <div class="col">
                      <div class="form-floating">
                        <input value="${user.numero_casa}" readonly=readonly type="text" class="form-control" id="housenumber" placeholder="No. de Casa">
                        <label for="housenumber">No. de Casa</label>
                      </div>
                    </div>
                      
                  </div>
      
                  <div class="row mb-3">
      
                    <div class="col">
                      <div class="form-floating">
                        <input value="${user.colonia}" readonly=readonly type="text" class="form-control" id="colna" placeholder="Colonia">
                        <label for="colna">Colonia</label>
                      </div>
                    </div>
      
                    <div class="col">
                      <div class="form-floating">
                        <input value="${user.codigo_postal}" readonly=readonly type="text" class="form-control" id="zipcode" placeholder="Código Postal">
                        <label for="zipcode">Código Postal</label>
                      </div>
                    </div>
      
                  </div>
      
                  <div class="row mb-3">
      
                    <div class="col">
                      <div class="form-floating">
                        <input value="${user.numero_local}" readonly=readonly type="text" class="form-control" id="localnumber" placeholder="Número Local">
                        <label for="localnumber">Número Local</label>
                      </div>
                    </div>
                  </div>
      
                </li>
              </ul>
            </form>
          </div>
        </div>
        `
        return aux
    }

    async function listAppointments(){
        let url= 'http://localhost/salonBelleza/public/fetchAppointmentsByUser';
        console.log(url);
        let obj;
        try{
            const res = await fetch(url);
            obj = await res.json();
            console.log(obj);
            document.getElementById('appointments-body').innerHTML = drawAppointments(obj);
        } catch (error){
            console.log(error);
        }
    }

    function drawAppointments(obj){
        let aux = ''
        
        obj.appointments.forEach(function(appointment){
            aux += `
            <tr>
                <td>${appointment.id_cita}</td>
                <td>${appointment.id_servicio}</td>
                <td>${appointment.fecha}</td>
                <td>${appointment.hora_inicio}</td>
                <td>${appointment.hora_fin}</td>
            </tr>
        `
        })
        
        return aux
    }


    listUserInfo()
    listAppointments()
</script>