<?=$common?>
<?=$header?>
<?=$menu?>

<style>
    .title{
        position: -webkit-sticky;
        position: sticky;
        z-index: 3;
        top: 0px;
        background-color: #cfcfcf;
        padding: 10px;
        border-radius: 5px 5px 5px 5px;
    }

    .success{
        position: center;
        z-index: 2;
        background-color: #FCC6C1;
        color: black;
        padding: 5px;
        border-radius: 5px 5px 5px 5px;
    }

    #hide-meA1 {
        width: 185px;
        height: 85px;
        background: #bf7130;
        display: none;
        padding: 10px;
        font-size: 12px;
        float: left;
        text-align: justify;
        background: rgba(0, 0, 0, .15);
        border-radius: 5px 5px 5px 5px;
    }
    #hide-meA2 {
        width: 185px;
        height: 85px;
        background: #bf7130;
        display: none;
        padding: 10px;
        font-size: 12px;
        float: left;
        text-align: justify;
        background: rgba(0, 0, 0, .15);
        border-radius: 5px 5px 5px 5px;
    }
    #hide-meA3 {
        width: 185px;
        height: 85px;
        background: #bf7130;
        display: none;
        padding: 10px;
        font-size: 12px;
        float: left;
        text-align: justify;
        background: rgba(0, 0, 0, .15);
        border-radius: 5px 5px 5px 5px;
    }
    #hide-meA4 {
        width: 185px;
        height: 85px;
        background: #bf7130;
        display: none;
        padding: 10px;
        font-size: 12px;
        float: left;
        text-align: justify;
        background: rgba(0, 0, 0, .15);
        border-radius: 5px 5px 5px 5px;
    }
    #hide-meB1 {
        width: 185px;
        height: 85px;
        background: #bf7130;
        display: none;
        padding: 10px;
        font-size: 12px;
        float: left;
        text-align: justify;
        background: rgba(0, 0, 0, .15);
        border-radius: 5px 5px 5px 5px;
    }
    #hide-meB2 {
        width: 185px;
        height: 85px;
        background: #bf7130;
        display: none;
        padding: 10px;
        font-size: 12px;
        float: left;
        text-align: justify;
        background: rgba(0, 0, 0, .15);
        border-radius: 5px 5px 5px 5px;
    }
    #hide-meB3 {
        width: 185px;
        height: 85px;
        background: #bf7130;
        display: none;
        padding: 10px;
        font-size: 12px;
        float: left;
        text-align: justify;
        background: rgba(0, 0, 0, .15);
        border-radius: 5px 5px 5px 5px;
    }
    #hide-meB4 {
        width: 185px;
        height: 85px;
        background: #bf7130;
        display: none;
        padding: 10px;
        font-size: 12px;
        float: left;
        text-align: justify;
        background: rgba(0, 0, 0, .15);
        border-radius: 5px 5px 5px 5px;
    }
    #hide-meC1 {
        width: 185px;
        height: 85px;
        background: #bf7130;
        display: none;
        padding: 10px;
        font-size: 12px;
        float: left;
        text-align: justify;
        background: rgba(0, 0, 0, .15);
        border-radius: 5px 5px 5px 5px;
    }
    #hide-meC2 {
        width: 185px;
        height: 85px;
        background: #bf7130;
        display: none;
        padding: 10px;
        font-size: 12px;
        float: left;
        text-align: justify;
        background: rgba(0, 0, 0, .15);
        border-radius: 5px 5px 5px 5px;
    }
    #hide-meC3 {
        width: 185px;
        height: 85px;
        background: #bf7130;
        display: none;
        padding: 10px;
        font-size: 12px;
        float: left;
        text-align: justify;
        background: rgba(0, 0, 0, .15);
        border-radius: 5px 5px 5px 5px;
    }
</style>

<h3 class="title">U Ñ A S</h3>
<div class="container px-8 py-5">
    <div class="row g-8 py-8 row-cols-1 row-cols-lg-4">
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesA1.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <div>
                            <h4>Spa en manos <button class="btn success" id="displayNoneA1">+</button></h4>
                            <div id="hide-meA1" class="float-none">
                                <a>Incluye aromaterapia mientras se ofrece el servicio, en el cual se aplican exfoliantes, cremas y al finalizar esmalte para tus uñas.</a>
                            </div>
                        </div>
                        <p></p>
                        <p>Tiempo: 60 minutos</p>
                        <p>Precio: Desde 399 pesos mxn</p>
                        
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/spa_manos').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div>   
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesA2.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <div>
                            <h4>Spa en pies <button class="btn success" id="displayNoneA2">+</button></h4>
                            <div id="hide-meA2"  class="float-none">
                                <a>Incluye aromaterapia mientras se ofrece el servicio, en el cual se aplican exfoliantes, cremas y al finalizar esmalte para tus uñas.</a>
                            </div>
                        </div>
                        <p> </p>
                        <p>Tiempo: 60 mins</p>
                        <p>Precio: Desde 449 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/spa_pies').'">A G E N D A R</a>';
                            } 
                        ?>
                        
                        <p></p>
                    </div>
                </div>
        </div> 
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesA3.jpeg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <h4>Manicure</h4>
                        <p>Tiempo: 40 minutos</p>
                        <p>Precio: 199 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/manicure').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div> 
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesA4.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <h4>Pedicure</h4>
                        <p>Tiempo: 40 minutos</p>
                        <p>Precio: 249 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/pedicure').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div> 
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesA5.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <div>
                            <h4>Uñas acrílicas <button class="btn success" id="displayNoneA3">+</button></h4>
                            <div id="hide-meA3"  class="float-none">
                                <a>Aplica para dos colores, puedes incluir diseño y/o pedreria con un costo extra.</a>
                            </div>
                        </div>
                        <p>Tiempo: 100 minutos</p>
                        <p>Precio: 499 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/unas_acrilicas').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div>   
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesA6.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <div>
                            <h4>Gelish <button class="btn success" id="displayNoneA4">+</button></h4>
                            <div id="hide-meA4"  class="float-none">
                                <a>Aplica para un solo color, puedes incluir más colores y/o diseño con un costo extra.</a>
                            </div>
                        </div>
                        <p>Tiempo: 60 minutos</p>
                        <p>Precio: 149 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/gelish').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div>    
    </div>
</div>

<h3 class="title">C A B E L L O</h3>
<div class="container px-8 py-5">
    <div class="row g-8 py-8 row-cols-1 row-cols-lg-4">
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesB1.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <h4>Corte de dama</h4>
                        <p>Tiempo: 30 minutos</p>
                        <p>Precio: Desde 199 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/corte_dama').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div>   
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesB2.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <h4>Corte de caballero</h4>
                        <p>Tiempo: 20 minutos</p>
                        <p>Precio: Desde 149 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/corte_caballero').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div> 
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesB3.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <h4>Corte para niños</h4>
                        <p>Tiempo: 30 minutos</p>
                        <p>Precio: Desde 159 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/corte_nino').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div> 
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesB4.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <h4>Alaciado</h4>
                        <p>Tiempo: 30 minutos</p>
                        <p>Precio: Desde 129 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/alaciado').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div> 
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesB5.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <div>
                            <h4>Permanente <button class="btn success" id="displayNoneB1">+</button></h4>
                            <div id="hide-meB1"  class="float-none">
                                <a>Cambia la estructura de tu cabello a como lo desees con esta técnica que dura entre 3 y 4 meses.</a>
                            </div>
                        </div>
                        <p>Tiempo: 150 minutos</p>
                        <p>Precio: Desde 2199 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/permanente').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div>   
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesB6.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <div>
                            <h4>Extensiones <button class="btn success" id="displayNoneB2">+</button></h4>
                            <div id="hide-meB2"  class="float-none">
                                <a>Extensiones de cabello natural 100%. Tiras adhesivas extra finas, montadas sobre cabello, que pueden durar hasta 3 meses.</a>
                            </div>
                        </div>
                        <p>Tiempo: 30 minutos</p>
                        <p>Precio: Desde 3499 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/extensiones').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div>  
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesB7.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <h4>Peinados</h4>
                        <p>Tiempo: 30 minutos</p>
                        <p>Precio: Desde 119 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/peinados').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div> 
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesB8.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <h4>Peinado Social</h4>
                        <p>Tiempo: 60 minutos</p>
                        <p>Precio: Desde 349 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/peinado_social').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div> 
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesB9.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <div>
                            <h4>Botox Capilar <button class="btn success" id="displayNoneB3">+</button></h4>
                            <div id="hide-meB3"  class="float-none">
                                <a>Tratamiento que aporta un extra de nutrición, reestruturación y brillo a tu pelo.</a>
                            </div>
                        </div>
                        <p>Tiempo: 80 minutos</p>
                        <p>Precio: Desde 999 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/botox_capilar').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div>  
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesB10.jpeg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <div>
                            <h4>Keratina Capilar <button class="btn success" id="displayNoneB4">+</button></h4>
                            <div id="hide-meB4"  class="float-none">
                                <a>Tratamiento que hidrata y da vitalidad al cabello, dando fuerza y brillo. La duración del alisado es de 3 a 6 meses.</a>
                            </div>
                        </div>
                        <p>Tiempo: 100 minutos</p>
                        <p>Precio: Desde 1799 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/keratina_capilar').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div> 
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesB11.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <h4>Tinte de cabello</h4>
                        <p>Tiempo: 90 minutos</p>
                        <p>Precio: Desde 599 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/tinte_cabello').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div> 
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesB12.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <h4>Tinte fantasía</h4>
                        <p>Tiempo: 60 minutos</p>
                        <p>Precio: Desde 399 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/tinte_fantasia').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div>
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesB13.jpeg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <h4>Balayage</h4>
                        <p>Tiempo: 150 minutos</p>
                        <p>Precio: Desde 1499 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/balayage').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div> 
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesB14.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <h4>Mechas</h4>
                        <p>Tiempo: 90 minutos</p>
                        <p>Precio: Desde 1199 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/mechas').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div> 
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesB15.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <h4>Retoque de color</h4>
                        <p>Tiempo: 40 minutos</p>
                        <p>Precio: Desde 349 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/retoque_color').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div>  
    </div>
</div>

<h3 class="title">R O S T R O</h3>
<div class="container px-8 py-5">
    <div class="row g-8 py-8 row-cols-1 row-cols-lg-4">
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesC1.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <div>
                            <h4>Facial <button class="btn success" id="displayNoneC1">+</button></h4>
                            <div id="hide-meC1"  class="float-none">
                                <a>Limpieza facial paso a paso de jabón limpiador, exfoliante, tónico facial, mascarillas y crema humectante.</a>
                            </div>
                        </div>
                        <p>Tiempo: 80 minutos</p>
                        <p>Precio: 279 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/facial').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div>   
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesC2.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <div>
                            <h4>Depilación con hilo <button class="btn success" id="displayNoneC2">+</button></h4>
                            <div id="hide-meC2"  class="float-none">
                                <a>Elimina vello facial desde la raíz sin dolor. Perfecto para pieles sensibles.</a>
                            </div>
                        </div>
                        <p>Tiempo: 15 minutos</p>
                        <p>Precio: Desde 159 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/depilacion_hilo').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div> 
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesC3.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <div>
                            <h4>Lifting de pestañas <button class="btn success" id="displayNoneC3">+</button></h4>
                            <div id="hide-meC3"  class="float-none">
                                <a>Tratamiento que alarga y crea una ligera curva hacia arriba de manera natural y dura entre 6 y 8 semanas.</a>
                            </div>
                        </div>
                        <p>Tiempo: 45 minutos</p>
                        <p>Precio: 799 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/lifting_pestanas').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div> 
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesC4.jpg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <h4>Maquillaje express</h4>
                        <p>Tiempo: 20 minutos</p>
                        <p>Precio: Desde 349 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/maquillaje_express').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div> 
        <div class="col">
                <div class="card">
                    <img class="card-img-top" src="http://localhost/salonBelleza/public/img/services/servicesC5.jpeg" style="object-fit: cover; object-position: center center; display: block;  top: 0; left: 0; width: 100%; max-width: 100%; height: 250px;" alt="Card image cap">
                    <div class="card-body">
                        <h4>Maquillaje social</h4>
                        <p>Tiempo: 70 minutos</p>
                        <p>Precio: Desde 749 pesos mxn</p>
                        <?php if(session('user')!=null){
                                echo '<a class="btn success" href="'.base_url('schedule/maquillaje_social').'">A G E N D A R</a>';
                            } 
                        ?>
                        <p></p>
                    </div>
                </div>
        </div>       
    </div>
</div>

<script src="js/jquery-3.6.0.min.js"></script>  
<script src="bootstrap.bundle.min.js"></script>  
<script src="js/sidebars.js"></script>

<script>
    $('#displayNoneA1').click(function(e) {
        if( $('#hide-meA1').is(":visible") ) {
            $('#hide-meA1').css('display', 'none'); 
        } else {
            $('#hide-meA1').css('display', 'block');
        }
    });

    $('#displayNoneA2').click(function(e) {
        if( $('#hide-meA2').is(":visible") ) {
            $('#hide-meA2').css('display', 'none'); 
        } else {
            $('#hide-meA2').css('display', 'block');
        }
    });

    $('#displayNoneA3').click(function(e) {
        if( $('#hide-meA3').is(":visible") ) {
            $('#hide-meA3').css('display', 'none'); 
        } else {
            $('#hide-meA3').css('display', 'block');
        }
    });

    $('#displayNoneA4').click(function(e) {
        if( $('#hide-meA4').is(":visible") ) {
            $('#hide-meA4').css('display', 'none'); 
        } else {
            $('#hide-meA4').css('display', 'block');
        }
    });

    $('#displayNoneB1').click(function(e) {
        if( $('#hide-meB1').is(":visible") ) {
            $('#hide-meB1').css('display', 'none'); 
        } else {
            $('#hide-meB1').css('display', 'block');
        }
    });

    $('#displayNoneB2').click(function(e) {
        if( $('#hide-meB2').is(":visible") ) {
            $('#hide-meB2').css('display', 'none'); 
        } else {
            $('#hide-meB2').css('display', 'block');
        }
    });

    $('#displayNoneB3').click(function(e) {
        if( $('#hide-meB3').is(":visible") ) {
            $('#hide-meB3').css('display', 'none'); 
        } else {
            $('#hide-meB3').css('display', 'block');
        }
    });

    $('#displayNoneB4').click(function(e) {
        if( $('#hide-meB4').is(":visible") ) {
            $('#hide-meB4').css('display', 'none'); 
        } else {
            $('#hide-meB4').css('display', 'block');
        }
    });

    $('#displayNoneC1').click(function(e) {
        if( $('#hide-meC1').is(":visible") ) {
            $('#hide-meC1').css('display', 'none'); 
        } else {
            $('#hide-meC1').css('display', 'block');
        }
    });

    $('#displayNoneC2').click(function(e) {
        if( $('#hide-meC2').is(":visible") ) {
            $('#hide-meC2').css('display', 'none'); 
        } else {
            $('#hide-meC2').css('display', 'block');
        }
    });

    $('#displayNoneC3').click(function(e) {
        if( $('#hide-meC3').is(":visible") ) {
            $('#hide-meC3').css('display', 'none'); 
        } else {
            $('#hide-meC3').css('display', 'block');
        }
    });
</script>

<?=$footer?>