

<style>

    
    button{
        border: none;
        width: 100%;
        height: 40px;
        background-color: #a76671;
        font-family: Dubai Light, Century Gothic;
        color: #fff;
        margin: 0;
        font-size: 20 px;
    }

    button2{
        border: none;
        width: 100%;
        height: 30px;
        background-color: transparent;
        font-family: Dubai Light, Century Gothic;
        text-align: center;
        color: #a76671;
        margin: 0;
        font-size: 0 px;
    }
</style>

<!-- container-fluid-->
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-3 col-lg-4">
        </div>    
        <div class="col-12 col-sm-6 col-lg-4">
            <br>
            <h1 style="text-align: center; font-family: Dubai Light, Century Gothic;" >Registro</h1>
            
            <p><?php
            if (session('error')!=null){
                echo '<ul><li style="color: #d56262;">'.session('error').'</li></ul>';
            } 
            ?> </p>
            <?=form_open('registerUser','class="needs-validation" novalidate');?>
                <div class="form-group">
                    <label style="font-family: Dubai Light, Century Gothic;" for="usuario" >Usuario</label>
                    <input type="text" class="form-control" placeholder="Usuario" id="usuario" name="usuario" required="">
                    <div class="invalid-feedback">
                        Usuario requerido.
                    </div>
                </div>
                <div class="form-group">
                    <label style="font-family: Dubai Light, Century Gothic;" for="password">Password</label>
                    <input type="password" class="form-control" placeholder="Password" id="password" name="password" required="">
                    <div class="invalid-feedback">
                        Contraseña requerida.
                    </div>
                </div>
                <div class="form-group">
                    <label style="font-family: Dubai Light, Century Gothic;" for="nombre">Nombre(s)</label>
                    <input type="text" class="form-control" placeholder="Nombre" id="nombre" name="nombre" required="">
                    <div class="invalid-feedback">
                        Nombre requerido.
                    </div>
                </div>
                <div class="form-group">
                    <label style="font-family: Dubai Light, Century Gothic;" for="apellido_paterno">Apellido paterno</label>
                    <input type="text" class="form-control" placeholder="Apellido paterno" id="apellido_paterno" name="apellido_paterno" required="">
                    <div class="invalid-feedback">
                        Apellido paterno requerido.
                    </div>
                </div>
                <div class="form-group">
                    <label style="font-family: Dubai Light, Century Gothic;" for="apellido_materno">Apellido materno</label>
                    <input type="text" class="form-control" placeholder="Apellido materno" id="apellido_materno" name="apellido_materno" required="">
                    <div class="invalid-feedback">
                        Apellido materno requerido.
                    </div>
                </div>
                <div class="form-group">
                    <label style="font-family: Dubai Light, Century Gothic;" for="alias">Alias</label>
                    <input type="text" class="form-control" placeholder="Alias" id="alias" name="alias">
                </div>
                <div class="form-group">
                    <label style="font-family: Dubai Light, Century Gothic;" for="calle">Calle</label>
                    <input type="text" class="form-control" placeholder="Calle" id="calle" name="calle" required="">
                    <div class="invalid-feedback">
                        Calle requerida.
                    </div>
                </div>
                <div class="form-group">
                    <label style="font-family: Dubai Light, Century Gothic;" for="numero_casa">Número de domicilio</label>
                    <input type="number" class="form-control" placeholder="Número de domicilio" id="numero_casa" name="numero_casa" required="">
                    <div class="invalid-feedback">
                        Número de domicilio requerido.
                    </div>
                </div>
                <div class="form-group">
                    <label style="font-family: Dubai Light, Century Gothic;" for="colonia">Colonia</label>
                    <input type="text" class="form-control" placeholder="Colonia" id="colonia" name="colonia" required="">
                    <div class="invalid-feedback">
                        Colonia requerida.
                    </div>
                </div>
                <div class="form-group">
                    <label style="font-family: Dubai Light, Century Gothic;" for="codigo_postal">Codigo postal</label>
                    <input type="text" class="form-control" placeholder="Codigo postal" id="codigo_postal" name="codigo_postal" required="">
                    <div class="invalid-feedback">
                        Código postal requerido.
                    </div>
                </div>
                <div class="form-group">
                    <label style="font-family: Dubai Light, Century Gothic;" for="correo_electronico">Correo electrónico</label>
                    <input type="email" class="form-control" placeholder="Correo electrónico" id="correo_electronico" name="correo_electronico" required="">
                    <div class="invalid-feedback">
                        Correo electrónico requerido.
                    </div>
                </div>
                <div class="form-group">
                    <label style="font-family: Dubai Light, Century Gothic;" for="numero_celular">Número de celular</label>
                    <input type="text" class="form-control" placeholder="Número de celular" id="numero_celular" name="numero_celular" required="">
                    <div class="invalid-feedback">
                        Número de celular requerido.
                    </div>
                </div>
                <div class="form-group">
                    <label style="font-family: Dubai Light, Century Gothic;" for="numero_local">Número teléfono local</label>
                    <input type="text" class="form-control" placeholder="Número teléfono local" id="numero_local" name="numero_local" required="">
                    <div class="invalid-feedback">
                        Número teléfono local requerido.
                    </div>
                </div>
                <div class="form">
                    <button type="submit">R E G I S T R A R</button>
                </div>
            <?=form_close()?>
        </div>
    </div>
</div>    

<script type="application/javascript">
    (function () {
        'use strict'
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')
        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
          .forEach(function (form) {
            form.addEventListener('submit', function (event) {
              if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
              }

              form.classList.add('was-validated')
            }, false)
          })
    })();
</script>