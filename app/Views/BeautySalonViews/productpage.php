<?=$common?>
<?=$header?>
<?=$menu?>

<div class="container">
    <div class="row">
        <p></p>
        <div class="col">
            <div class="container-fluid">
                <div id="productCarousel" class="carousel slide carousel-fade" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button class="active" type="button" data-bs-target="#productCarousel" data-bs-slide-to="0" aria-label="Slide 1" aria-current="true"></button>
                        <button class="" type="button" data-bs-target="#productCarousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button class="" type="button" data-bs-target="#productCarousel" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner" id="fotos">
                        <div class="carousel-item active ">
                            <img class="d-block w-100 rounded border" src="<?=base_url('img/productEx1.jpg')?>" alt="Slide1">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100 rounded border" src="<?=base_url('img/productEx2.jpg')?>" alt="Slide2">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100 rounded border" src="<?=base_url('img/productEx3.jpg')?>" alt="Slide3">
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#productCarousel" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Anterior</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#productCarousel" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Siguiente</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="col" id="product-info">
        </div>
        <p></p>
    </div>
</div>

<script>
    async function listProduct(id){
        let url= 'http://localhost/salonBelleza/public/fetchProduct/'+id;
        console.log(url);
        let obj;
        try{
            const res = await fetch(url);
            obj = await res.json();
            console.log(obj);
            document.getElementById('fotos').innerHTML = drawProductPhotos(obj);
            document.getElementById('product-info').innerHTML = drawProduct(obj);
        } catch (error){
            console.log(error);
        }
    }

    function drawProductPhotos(obj){
        let product_photos = ''
        product1 = obj.product
        
        product_photos += `
            <div class="carousel-item active ">
                <img class="d-block w-100 rounded border" src="<?=base_url('img/products')?>/${product1.foto1}" alt="Slide1">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100 rounded border" src="<?=base_url('img/products')?>/${product1.foto2}" alt="Slide2">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100 rounded border" src="<?=base_url('img/products')?>/${product1.foto3}" alt="Slide3">
            </div>
        `
        return product_photos
    }

    function drawProduct(obj){
        let product_info_inner = ''
        product1 = obj.product
        
        product_info_inner += `
            <ol class="breadcrumb"> 
                <li class="breadcrumb-item"><a href="<?=base_url('homepage')?>" style="color: #3d3b3b;">Inicio</a></li>
                <li class="breadcrumb-item"><a href="<?=base_url('products')?>" style="color: #3d3b3b;">Productos</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="<?=base_url('productpage')?>/${product1.id_producto}" style="color: #3d3b3b;">${product1.nombre}</a></li>
                
            </ol> 
            <h1>${product1.nombre}</h1>
            <p>${product1.descripcion}</p>
            <form method="post" action="<?=base_url('addProduct')?>/${product1.id_producto}">
                <div>                                        
                    <select name="cantidad">
                        <optgroup label="Cantidad">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </optgroup>
                    </select>
                    <br>
                    <span class="text-dark">Precio $${product1.precio}</span>
                    <br><br>
                </div>
                <div>
                    <button class="btn btn-primary btn-sm" type="submit"><i class="fa-solid fa-plus"></i>  Add</button>
                </div>
            </form>
        `
        return product_info_inner
    }

    listProduct(<?=$id?>);
</script>

<?=$footer?>