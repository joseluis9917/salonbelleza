<nav class="main-header navbar navbar-expand navbar-light navbar-white">
    <div class="container">
        <a class="navbar-brand" href="<?=base_url('homepage')?>">
            <img src="<?=base_url('img/logo-xs-bp.png')?>" alt="Beauty Salon Logo" class="brand-image img-circle" style="opacity: : .8;">
            <span class="brand-text font-weight-light">BeautyCare</span>
        </a>

        <ul class="navbar-nav">
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?=base_url('homepage')?>" class="nav-link"><!--<i class="fa-solid fa-house"></i>-->Inicio</a>
            </li>
            <div class="vr"></div>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?=base_url('gallery')?>" class="nav-link"><!--<i class="fa-solid fa-image"></i>-->Galeria</a>
            </li>
            <div class="vr"></div>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?=base_url('products')?>" class="nav-link"><!--<i class="fa-solid fa-bag-shopping"></i>-->Productos</a>
            </li>
            <div class="vr"></div>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?=base_url('services')?>" class="nav-link"><!--<i class="fa-solid fa-scissors"></i>-->Servicios</a>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link" data-bs-toggle="dropdown" href="#">
                    <i class="fa-solid fa-user"></i>
                </a>
                <div class="dropdown-menu">
                        <?php if(session('user')!=null){
                            if (session('type') == 'admin'){
                                echo '<a href='.base_url('adminprofile').' class="dropdown-item">'.session('user').'</a>';
                            } else {
                                echo '<a href='.base_url('userprofile').' class="dropdown-item">'.session('user').'</a>';
                            }
                                
                            } else {
                                echo '<a href='.base_url('loginpage').' class="dropdown-item">
                                        <i class="fa-solid fa-right-to-bracket"></i>Entrar
                                    </a>';
                            }
                        ?>
                    
                    <div class="dropdown-divider"></div>
                        <?php if(session('user')!=null){
                                echo '<a href='.base_url('logout').' class="dropdown-item">
                                        </i>Salir
                                    </a>';
                            } else {
                                echo '<a href='.base_url('registerpage').' class="dropdown-item">
                                        <i class="fa-solid fa-user-plus"></i>Registrarse
                                    </a>';
                            }
                        ?>
                </div>
            </li>
            <!--<li class="nav-item dropdown">-->
                <a class="nav-link" href="<?=base_url('shoppingcart')?>">
                    <i class="fa-solid fa-cart-shopping">
                        <span class="badge badge-danger navbar-badge">
                            <?php if(session('user')!=null){
                                echo count(session('products'))+count(session('appointments'));
                            } else {
                                echo '0';  
                            }?>
                        </span>
                    </i>
                </a>
            <!--</li>-->
        </ul>
    </div>
</nav>

<body class="layout-top-nav" style="width: 100%; position: absolute; height:auto !important; min-height: 100%;">
  <div class="wrapper">