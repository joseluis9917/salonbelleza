</div>
<footer class="main-footer">
    <div class="container">
        <div class="row">
            <div class="col-4 footer_title">
               <strong>Sobre nosotros<br></strong>
            </div>
            <div class="col-4 footer_title">
                <strong>Servicio al cliente<br></strong>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                Información de la compañia
            </div>
            <div class="col-4">
                <a href="<?=base_url("feedbackPage")?>">Contacto</a> 
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                Politica de privacidad
            </div>
            <div class="col-4">
                Preguntas frecuentes
            </div>
        </div>
        <br><br>
    </div>
    <hr class="footer_hr"/>
    <div class="float-right d-none d-sm-inline">BeautyCare Salon ®</div>
    <strong>
        Copyright © 2019-2023.
    </strong>
    All rights reserved.
</footer>
<!--</div>-->



<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url('/dist/twbs/bootstrap/dist/js/bootstrap.bundle.js') ?>"></script>
<script src="<?php echo base_url('/dist/almasaeed2010/adminlte/dist/js/adminlte.js') ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.7/jquery.inputmask.min.js"></script>
</div>
</body>


</html>
