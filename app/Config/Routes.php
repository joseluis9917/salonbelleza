<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'BeautySalon::index');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}

/* Homepage */
$routes->get('homepage', 'BeautySalon::index');

/* Product page */
$routes->get('products', 'BeautySalon::products');
$routes->get('fetchProducts/(:num)', 'BeautySalon::fetchProductsByCategory/$1');

/* Individual product page */
$routes->get('productpage/(:num)', 'BeautySalon::productpage/$1');
$routes->get('fetchProduct/(:num)', 'BeautySalon::fetchProductByID/$1');

/* Feedback page */
$routes->get('feedbackPage', 'BeautySalon::feedbackPage');
$routes->get('feedbackSuccess', 'BeautySalon::feedbackSuccess');
$routes->post('proccessFeedback', 'BeautySalon::proccessFeedback');

/* Schedule page */
$routes->get('schedule/(:any)', 'BeautySalon::schedule/$1');
$routes->get('scheduletime/(:any)', 'BeautySalon::scheduletime/$1');
$routes->post('proccessAppointment/(:any)', 'BeautySalon::proccessAppointment/$1');
$routes->post('proccessDate/(:any)', 'BeautySalon::proccessDate/$1');

/* Login page */
$routes->get('loginpage', 'BeautySalon::loginpage');
$routes->post('processLogin', 'BeautySalon::processLogin');

/* Logout page */
$routes->get('logout', 'BeautySalon::logout');

/* Register page */
$routes->get('registerpage', 'BeautySalon::registerpage');
$routes->post('registerUser', 'BeautySalon::registerUser');

/* Gallery page */
$routes->get('gallery', 'BeautySalon::gallery');

/* Shopping cart page */
$routes->get('shoppingcart', 'BeautySalon::shoppingcart');
$routes->post('addProduct/(:num)', 'BeautySalon::addProduct/$1');
$routes->get('removeProduct/(:num)', 'BeautySalon::removeProduct/$1');
$routes->get('removeAppointment/(:any)', 'BeautySalon::removeAppointment/$1');
$routes->get('fetchCartProducts', 'BeautySalon::fetchCartProducts');
$routes->get('fetchAppointments', 'BeautySalon::fetchAppointments');
$routes->get('processOrder/(:any)', 'BeautySalon::processOrder/$1');
$routes->get('calculateDiscount/(:any)', 'BeautySalon::calculateDiscount/$1');

/* Services page */
$routes->get('services', 'BeautySalon::services');

/* User profile */
$routes->get('userprofile', 'BeautySalon::userprofile');
$routes->get('adminprofile', 'BeautySalon::adminprofile');
$routes->get('fetchUser', 'BeautySalon::fetchUser');
$routes->get('fetchAppointmentsByUser', 'BeautySalon::fetchAppointmentsByUser');
$routes->get('fetchAdminInfo', 'BeautySalon::fetchAdminInfo');